<div align="center" markdown="1">

# [`snippets-javascript-assignments`][url-repo]

[![License][shield-license]][url-license]
[![Status][shield-status-finished]][url-repo]

Javascript assignments for a course

</div>

## About The Project

### Features

- Boring
- Basic javascript, html, css
- Uses github pages - https://shishifubing.com/snippets-javascript-assignments

### Contents

- [ajax] - `C5`
- [browser_api] - `C6`
- [objects] - `C3`

<!-- relative links -->

[ajax]: ./ajax/
[browser_api]: ./browser_api/
[objects]: ./objects/

<!-- project links -->

[url-repo]: https://github.com/shishifubing/snippets-javascript-assignments
[url-license]: https://github.com/shishifubing/snippets-javascript-assignments/blob/main/LICENSE

<!-- external links -->

<!-- shield links -->

[shield-status-finished]: https://img.shields.io/badge/status-finished-informational?style=for-the-badge
[shield-license]: https://img.shields.io/github/license/shishifubing/snippets-javascript-assignments.svg?style=for-the-badge
